const enviar = document.querySelector('#enviar');
const formulario = document.querySelector('#enviar-mail');
const resetear = document.querySelector('#resetBtn');

const email = document.querySelector('#email');
const asunto = document.querySelector('#asunto');
const mensaje = document.querySelector('#mensaje');

const cajaError = document.querySelector('#error');

const er = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

eventListeners();
function eventListeners() {
  document.addEventListener('DOMContentLoaded', iniciarApp);
  email.addEventListener('input', validarFormulario);
  asunto.addEventListener('input', validarFormulario);
  mensaje.addEventListener('input', validarFormulario);
  email.addEventListener('blur', validarFormulario);
  asunto.addEventListener('blur', validarFormulario);
  mensaje.addEventListener('blur', validarFormulario);

  formulario.addEventListener('submit', enviarMensaje); 

  resetear.addEventListener('click', (e) => {
    e.preventDefault();
    limpiarError();
    limpiarFormulario();
  });
}

function iniciarApp() {
  // Se desabilita el boton enviar
  enviar.disabled = true;
  enviar.classList.add('cursor-not-allowed', 'opacity-50');
}

function validarFormulario(e) {
  let contenido = true;
  let patron = true; 

  if (e.target.value.trim().length > 0) {
    contenido = true;
  } else {
    invalido(e.target);
    mostrarError('Todos los campos son obligatorios');
    contenido = false;
  }
  // Validar email
   if (e.target.type === 'email') {
    if (er.test(e.target.value)) {
      patron = true;
    } else {
      invalido(e.target);
      mostrarError('No es un correo');
      patron = false;
    }
  }
  // Reovemos mensaje de error cuando se cumplan todas las validadciones
  if (contenido && patron) {
    valido(e.target);
    limpiarError();
  }
  // Validamos todo el formulario
  if (er.test(email.value) && asunto.value.trim() !== '' && mensaje.value.trim()  !== '') {
    enviar.disabled = false;
    enviar.classList.remove('cursor-not-allowed', 'opacity-50');
  } else {
    iniciarApp();
  }
}

function invalido(elemento) {
  elemento.classList.add('border');
  elemento.classList.add('border-red-500');
  elemento.classList.remove('border-green-500');
}
function valido(elemento) {
  elemento.classList.add('border');
  elemento.classList.remove('border-red-500');
  elemento.classList.add('border-green-500');
}
function remover(elemento) {
  elemento.classList.add('border');
  elemento.classList.remove('border-red-500');
  elemento.classList.remove('border-green-500');
  elemento.classList.add('border-gray-100');
}

function mostrarError(texto) {
  limpiarError();
  const mensaje = document.createElement('p');
  mensaje.textContent = texto;
  mensaje.classList.add('border', 'border-red-500', 'text-red-500', 'p-3', 'error');
  cajaError.appendChild(mensaje);
}
function limpiarError() {
  while(cajaError.firstChild) {
    cajaError.removeChild(cajaError.firstChild);
  }
}
function enviarMensaje(e) {
  e.preventDefault();
  //alert("mensaje enviado bebé ;)")
  const spinner = document.querySelector('#spinner');
  spinner.style.display = 'flex';

  setTimeout(() => {
    spinner.style.display = 'none';
    const enviado = document.createElement('p');
    enviado.textContent = 'Mensaje enviado ;)';
    enviado.classList.add('border', 'border-green-500', 'text-green-500', 'p-3', 'success', 'text-center');
    formulario.insertBefore(enviado, spinner);

    setTimeout(() => {
      formulario.removeChild(enviado);
      limpiarFormulario();
    }, 1000);
  }, 2000);
}
function limpiarFormulario() {
  formulario.reset();
  remover(email);
  remover(asunto);
  remover(mensaje);
  iniciarApp();
}